# Build Admin Portal UI
FROM node:latest as admin-module

WORKDIR /admin-ui

ARG BRANCH_NAME=main
ARG BASE_HREF=/

#RUN apk update \
# && add wget unzip

# Download admin-portal; from specific branch (main by default) build for production
RUN wget 'https://gitlab.com/divolte/admin-portal/-/archive/main/admin-portal-main.zip' -O admin-portal-main.zip && \
    unzip admin-portal-main.zip && \
    rm admin-portal-main.zip && \

    cd admin-portal-main && \
    # Install dependencies and build it for production
    npm install && \
    npm run build --prod


# Stage 2
FROM nginx:latest

WORKDIR /admin-ui

COPY --from=admin-module /admin-ui/admin-portal-main/dist /usr/share/nginx/html
