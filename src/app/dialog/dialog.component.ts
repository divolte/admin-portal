import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  @Input()
  name = "";
  @Input()
  message = "";
  @Input()
  heading = "";
  @Input()
  cancelEventId=""
  @Input()
  close: ()=>void;
  @Input()
  signout:()=>void;
  @Input()
  confirmCancel:(event)=>void;


  constructor() { }


  ngOnInit(): void {
  }

}
