export interface adminTypeCustomer{
    "type-of-customer": String,
    "max-annual-premium": Number,
    "min-annual-premium": Number,
    "max-total-sum-assured": Number,
    "min-total-sum-assured": Number,
}