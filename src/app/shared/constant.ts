export const dialogData = {
    name: '',
    message: '',
    heading: ''
}

export const isLoading=false;
export const showDialog=false;
export const showCancel = false;
export const cancelEventId ='';

export const sectionId={
    CUSTOMER_TYPE_CONFIG:'CUSTOMER_TYPE_CONFIGURAITON',
    USE_CASE_CONFIGURATION_PARAMTERS: 'USE_CASE_CONFIGURATION_PARAMTERS',
    COMMUNICATION_TRIGGERS: 'COMMUNICATION_TRIGGERS'
}
export const intialJsonCustomerType = {
    HNI_AP_min: 0, 
    HNI_AP_max: 0,
    HNI_TSA_min: 0,
    HNI_TSA_max: 0,
    MED_AP_min: 0,
    MED_AP_max: 0,
    MED_TSA_min: 0,
    MED_TSA_max: 0,
    LOW_AP_min: 0,
    LOW_AP_max: 0,
    LOW_TSA_min: 0,
    LOW_TSA_max: 0
  }

  export const useCaseFunctionalityMapping = {
    'UC_3':{
      'F100': 'UC3F100',
      'F101': 'UC3F101',
      'F102': 'UC3F102',
      'F103': 'UC3F103',
      'F104': 'UC3F104',
      'F106': 'UC3F106',
      'F107': 'UC3F107',
      'F109': 'UC3F109',
      'F110': 'UC3F110',
      'F114': 'UC3F113',
      'F113': 'UC3F114',
      'F120': 'UC3F120',
      'F122': 'UC3F122',
    },
    'UC_4':{
      'F101': 'UC4F101',
      'F103': 'UC4F103',
      'F114': 'UC4F114',
      'F120': 'UC4F120',
      'F122': 'UC4F122',
      'F133': 'UC4F133',
      'F129': 'UC4F129',
      'F130': 'UC4F130',
    },
    'UC_6':{
      'F100': 'UC6F100',
      'F103': 'UC6F103',
      'F106': 'UC6F106',
      'F102': 'UC6F102',
      'F114': 'UC6F114',
      'F101': 'UC6F101',
    },
    'UC_40':{
      'F001':'UC40F001', 
    },
    'UC_41':{
      'F001':'UC41F001'
    }

  }

  export const commTriggerMapping = {
    'UC_8':{
      'F120': 'UC8F120'  //Unclaimed Fund
    },
    'UC_27':{
      'F132':'UC27F132'
    },
    'UC_13':{
      'F001':'UC13F001'
    },
    'UC_14':{
      'F001':'UC14F001'
    },
    'UC_21':{
      'F001':'UC21F001'
    },
    'UC_29':{
      'F129':'UC29F129'
    },
    'UC_18':{
      'F001':'UC18F001'
    }
  }

export const optionDays = [{value: '1',name: '1 Day', selected: false},
                {value: '5',name: '5 Days', selected: false},
                {value: '7',name: '7 Days', selected: false},
                {value: '10',name: '10 Days', selected: false},
                {value: '15',name: '15 Days', selected: false},
                {value: '30',name: '30 Days', selected: false},
                {value: '45',name: '45 Days', selected: false},
                {value: '60',name: '60 Days', selected: false},
              ]
export const optionDropdown = [{value: '1',name: '1 Day', selected: false},
  {value: '2',name: '2 Days', selected: false},
  {value: '7',name: '7 Days', selected: false},
  {value: '15',name: '15 Days', selected: false},
  {value: '30',name: '1 Month', selected: false},
  {value: '60',name: '2 Month', selected: false},
  {value: '90',name: '3 Month', selected: false},
]
export const formEnable ={
    'CUSTOMER_TYPE_CONFIGURAITON': false,
    'USE_CASE_CONFIGURATION_PARAMTERS': false,
    'COMMUNICATION_TRIGGERS': false,
  }
