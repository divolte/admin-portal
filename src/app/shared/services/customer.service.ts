import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  secretKey = "dGFsaWNkYXNoYm9hcmQwMHRhbGljZGFzaGJvYXJkMDA="; //Base 64 encoded data
  // serverUrl = 'https://api-dev.tataaia.com/uat-interaction/ssp/admin/'
  serverUrl = 'http://172.23.30.72/admin/ssp/admin/'

  constructor(private http: HttpClient) {}
 //Encrypt Decrypt Function
  encrypt(textToEncrypt : string) : string{

    var parsedKey = CryptoJS.enc.Base64.parse(this.secretKey);
    //encoded Key
    var encryptedData = CryptoJS.AES.encrypt(textToEncrypt,parsedKey, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
      } );
      return encryptedData.toString();
  }
  decrypt(textToDecrypt : string){
    var parsedKey = CryptoJS.enc.Base64.parse(this.secretKey);
    var decryptedData = CryptoJS.AES.decrypt(textToDecrypt, parsedKey, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.pkcs7
    })
    return decryptedData.toString();

  }

  getHeader(){
   return  new HttpHeaders({ 'Authentication': "Bearer "+ this.getAuthenticationToken(),'Content-Type':'application/json'})
  }
  getAuthenticationToken(){
    const token = localStorage.getItem('currentAuthenticationToken');
    return token;
  }
  getAuthorizationToken(){
    const token = localStorage.getItem('currentAuthorizationToken');
    return token;
  }

  getAuthorization(){
    return this.http.get<any>(`${this.serverUrl}auth`);
  }

  postLogin(username: String, password: String){
    const headers = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    username = this.encrypt(String(username)).toString();
    password = this.encrypt(String(password)).toString();
    var urlencoded = new URLSearchParams();
    urlencoded.append("username", encodeURIComponent(username.toString()));
    urlencoded.append("password", encodeURIComponent(password.toString()));
    return this.http.post<any>(`${this.serverUrl}login`, urlencoded, {headers:headers});
  }

  getAdminDetails(){
    // const headers = new HttpHeaders({'Authorization': "Bearer "+ this.getCurrentUserToken(),'Content-Type':'application/json'})
    const headers = this.getHeader();

    return this.http.get<any>(`${this.serverUrl}admin-details`, {headers: headers});
  }

  getCustomerData(){
    // const headers = new HttpHeaders({'Authorization': "Bearer "+ this.getCurrentUserToken(),'Content-Type':'application/json'})
    const headers = this.getHeader();
    return this.http.get<any>(`${this.serverUrl}type-of-customer`, {headers: headers});
  }
  getuseCaseData(){
    // const headers = new HttpHeaders({'Authorization': "Bearer "+ this.getCurrentUserToken(),'Content-Type':'application/json'})
    const headers = this.getHeader();
    return this.http.get<any>(`${this.serverUrl}count-saved-data`, {headers: headers});
  }
  getcommTriggerData(){
    // const headers = new HttpHeaders({'Authorization': "Bearer "+ this.getCurrentUserToken(),'Content-Type':'application/json'})
    const headers = this.getHeader();
    return this.http.get<any>(`${this.serverUrl}communication-trigger`, {headers: headers});
  }


  putCustomerFormData(data){
    // const headers = new HttpHeaders({'Authorization': "Bearer "+ this.getCurrentUserToken(),'Content-Type':'application/json'})
    const headers = this.getHeader();
    const reqBody = {
      username: localStorage.getItem('username')?localStorage.getItem('username'):'',
      "admin-type-customer": data
    }
    return this.http.put(`${this.serverUrl}type-of-customer-change`,reqBody,{headers: headers})
  }
  putUseCaseFormData(data){
    // const headers = new HttpHeaders({
    //   'Authorization': "Bearer "+ this.getCurrentUserToken(),
    //   'Content-Type':'application/json',
    // })
    const headers = this.getHeader();
    const reqBody = {
      username: localStorage.getItem('username')?localStorage.getItem('username'):'',
      'admin-count-list': data
    }
    return this.http.put(`${this.serverUrl}use-case-count-value`,reqBody,{headers: headers})
  }

  putCommTriggerFormData(data){
    // const headers = new HttpHeaders({'Authorization': "Bearer "+ this.getCurrentUserToken(),'Content-Type':'application/json'})
    const headers = this.getHeader();
    const reqBody = {
      username: localStorage.getItem('username')?localStorage.getItem('username'):'',
      ...data
    }

    return this.http.put(`${this.serverUrl}ccm-Communication`,reqBody,{headers: headers})

  }

  postSignout(){
    localStorage.removeItem('currentAuthenticationToken');
    localStorage.removeItem('currentAuthorizationToken');
    localStorage.removeItem('username');
  }
}
