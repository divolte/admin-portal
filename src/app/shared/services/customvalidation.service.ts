import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomvalidationService {

  constructor() { }

  numberValidator(): ValidatorFn{
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return {invalidValue:false};
      }
      const regex = new RegExp('^[0-9]+$');
      const valid = regex.test(control.value);
      return valid ? {valid: true} : { invalidValue: true };
    };
  }
  validateNumber(control: AbstractControl){
    if(!/^[0-9]+$/.test(control.value)){
      return {validNumber: true}
    }
    return null;
  }

  // numberValidator1(
  //   control: AbstractControl
  // ): { [key: string]: boolean } | null {
  //     const regex = new RegExp('^[0-9]+$');
  //     const valid = regex.test(control.value);
  //     if(!valid){
  //       return {invalidValue:true}
  //     }
      
  //   return null;
  // }
}
