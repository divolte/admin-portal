import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl, FormArray} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../shared/services/customer.service';
import MODAL_TYPE from '../../assets/modal.type.json';
import { CustomvalidationService } from '../shared/services/customvalidation.service';
import { adminTypeCustomer } from '../shared/models/customer.model';
import { adminCount } from '../shared/models/adminCount.model';
import { commTrigger } from '../shared/models/commTrigger.model';
import {dialogData, isLoading, showDialog, showCancel, cancelEventId, sectionId, intialJsonCustomerType,useCaseFunctionalityMapping, commTriggerMapping, optionDays, optionDropdown, formEnable} from '../shared/constant';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})

export class CustomerComponent implements OnInit {

  //Declarations
  dialogData = dialogData
  isLoading=isLoading;
  showDialog=showDialog;
  showCancel = showCancel;
  cancelEventId =cancelEventId;

  //Form Declarations
  customerTypeConfigForm: FormGroup;
  configParameterForm: FormGroup;
  commTriggerForm: FormGroup;

  sectionId=sectionId;

  intialJsonCustomerType = intialJsonCustomerType

  useCaseFunctionalityMapping = useCaseFunctionalityMapping

  commTriggerMapping = commTriggerMapping

  optionDays = optionDays
  optionDropdown = optionDropdown
  formEnable = formEnable

  constructor(
    private fb: FormBuilder, 
    private customerService: CustomerService,
    private customerValidation: CustomvalidationService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.createForm();
    this.customerService.getAdminDetails().subscribe(
      {
        next:(response)=>{
          if(response?.status.toLowerCase() == 'success'){
            this.isLoading=false;
            this.insertValuesIntoCustomerForm(response.response?.["admin-type-customer-list"]);
            this.insertValuesIntoUseCaseForm(response.response?.["admin-count-list"]);
            this.insertValuesIntocommTriggerForm(response.response?.["communication-trigger-list"]);
          }
          else {
            this.isLoading=false;
            this.showDialog = true;
            this.dialogData.name = MODAL_TYPE.DATA_FETCH_ERROR.name
            this.dialogData.heading =  MODAL_TYPE.DATA_FETCH_ERROR.heading
            this.dialogData.message =  MODAL_TYPE.DATA_FETCH_ERROR.message
          }
          },
        error: (err)=>{
          this.isLoading=false;
          this.showDialog = true;
          this.dialogData.name = MODAL_TYPE.DATA_FETCH_ERROR.name
          this.dialogData.heading =  MODAL_TYPE.DATA_FETCH_ERROR.heading
          this.dialogData.message =  MODAL_TYPE.DATA_FETCH_ERROR.message
          }});
  }

  createForm(){    
    this.customerTypeConfigForm=this.fb.group({
      HNI_AP_min: new FormControl({value: this.intialJsonCustomerType.HNI_AP_min, disabled: true},[this.customerValidation.validateNumber]),
      HNI_AP_max: new FormControl({value: this.intialJsonCustomerType.HNI_AP_max, disabled: true},[ this.customerValidation.validateNumber]),
      HNI_TSA_min: new FormControl({value: this.intialJsonCustomerType.HNI_TSA_min, disabled: true},[ this.customerValidation.validateNumber]),
      HNI_TSA_max: new FormControl({value: this.intialJsonCustomerType.HNI_TSA_max, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      MED_AP_min: new FormControl({value: this.intialJsonCustomerType.MED_AP_min, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      MED_AP_max: new FormControl({value: this.intialJsonCustomerType.MED_AP_max, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      MED_TSA_min: new FormControl({value: this.intialJsonCustomerType.MED_TSA_min, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      MED_TSA_max: new FormControl({value: this.intialJsonCustomerType.MED_TSA_max, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      LOW_AP_min: new FormControl({value: this.intialJsonCustomerType.LOW_AP_min, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      LOW_AP_max: new FormControl({value: this.intialJsonCustomerType.LOW_AP_max, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      LOW_TSA_min: new FormControl({value: this.intialJsonCustomerType.LOW_TSA_min, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
      LOW_TSA_max: new FormControl({value: this.intialJsonCustomerType.LOW_TSA_max, disabled: true},[Validators.required, this.customerValidation.validateNumber]),
    })
    this.configParameterForm=this.fb.group({
      UC_3: this.fb.group({
        UC3F100: new FormControl(""), //Mobile Number Update
        UC3F101: new FormControl(""), //Address Change
        UC3F102: new FormControl(""), //Email ID update
        UC3F103: new FormControl(""), //Pan Update
        UC3F104: new FormControl(""), //Premium Due Alert
        UC3F106: new FormControl(""), //Nominee Change
        UC3F107: new FormControl(""), //Nav Alert
        UC3F109: new FormControl(""), //Preffered Communication Mode
        UC3F110: new FormControl(""), //Preffered Communication Language
        UC3F113: new FormControl(""), //Link my family policy
        UC3F114: new FormControl(""), //NEFT Update
        UC3F120: new FormControl(""), //Unclaimed Fund
        UC3F122: new FormControl(""), //Grievance Redressal
      }),
      UC_4: this.fb.group({
        UC4F101: new FormControl(""), //Address Change
        UC4F103: new FormControl(""), //PAN Update
        UC4F114: new FormControl(""), //NEFT Update
        UC4F120: new FormControl(""), //Unclaimed Fund
        UC4F122: new FormControl(""), //Grievance Redressal
        UC4F133: new FormControl(""), //Raise SR
        UC4F129: new FormControl(""), //Annuity Certificate
        UC4F130: new FormControl(""), //GST Waiver
      }),
      UC_6: this.fb.group({
        UC6F100: new FormControl(""), //Mobile Number Update
        UC6F103: new FormControl(""), //PAN Update
        UC6F106: new FormControl(""), //Nominee Changee
        UC6F102: new FormControl(""), //Email ID Update
        UC6F114: new FormControl(""), //NEFT Update
        UC6F101: new FormControl(""), //Address Change
      }),
      UC_40: this.fb.group({
        UC40F001: new FormControl("") //NPS Score
      }),
      UC_41: this.fb.group({
        UC41F001: new FormControl("") //Count of SR
      })
    })
    this.commTriggerForm= this.fb.group({
      UC_8:this.fb.group({
        UC8F120: new FormControl(0,[Validators.min(1),Validators.max(365)]), //Unclaimed Fund
        UC_8: new FormControl(null)
      }),
      UC_27:this.fb.group({
        UC27F132: this.fb.array(this.optionDays.map(x=>x.selected?x.value:null)), //Portal Vesting Age
        UC_27: new FormControl(null)
      }),
      UC_13:this.fb.group({
        UC13F001: new FormControl(0,[Validators.min(1), Validators.max(30)]), //Quote generator premium calculator
        UC_13: new FormControl(null)
      }),
      UC_14: this.fb.group({
        UC14F001: new FormControl(0,[Validators.min(1), Validators.max(30)]), //Product Recommendation
        UC_14: new FormControl(null)
      }),
      UC_21: this.fb.group({
        UC21F001: new FormControl(0,[Validators.min(1), Validators.max(15)]), //Document pending for upload
        UC_21:new FormControl(null)
      }),
      UC_29: this.fb.group({
        UC29F129: this.fb.array(this.optionDays.map(x=>x.selected?x.value:null)),
        UC_29: new FormControl(null)
      }),
      UC_18: this.fb.group({
        UC18F001: new FormControl(''),
        UC_18: new FormControl(null)
      }),
      UC_01: this.fb.group({
        UC_01: new FormControl(null)
      })
    })

    this.commTriggerForm.disable();
    this.configParameterForm.disable();
  }

  insertValuesIntoCustomerForm(customerList: any){
    console.log(typeof customerList);
    console.log(JSON.stringify(customerList))
    for(let customerIndex: number =0;customerIndex <customerList.length;customerIndex++){
      if(customerList[customerIndex].hasOwnProperty('type-of-customer') ){
        let customerType = customerList[customerIndex]['type-of-customer'].toUpperCase().slice(0,3);
          if(['MED', 'HNI', 'LOW'].includes(customerType)){
          this.customerTypeConfigForm.controls[`${customerType}_AP_max`].setValue(customerList[customerIndex]['max-annual-premium']);
          this.customerTypeConfigForm.controls[`${customerType}_AP_min`].setValue(customerList[customerIndex]['min-annual-premium']);
          this.customerTypeConfigForm.controls[`${customerType}_TSA_max`].setValue(customerList[customerIndex]['max-total-sum-assured']);
          this.customerTypeConfigForm.controls[`${customerType}_TSA_min`].setValue(customerList[customerIndex]['min-total-sum-assured']);
        }
      }
    }
  }

  insertValuesIntoUseCaseForm(useCaseList: any){
    for(let useCaseIndex: number =0; useCaseIndex < useCaseList.length; useCaseIndex++){
      if(useCaseList[useCaseIndex].hasOwnProperty('use-case-id') && useCaseList[useCaseIndex].hasOwnProperty('functionality-id')){
        let useCaseId = useCaseList[useCaseIndex]['use-case-id'];
        let functionalityId = useCaseList[useCaseIndex]['functionality-id'];
        let functionControl = this.useCaseFunctionalityMapping[`${useCaseId}`][`${functionalityId}`];
        let value = useCaseList[useCaseIndex]['count'];
        value = value != null? String(value): undefined;
        
        if(!isNaN(value)){
        this.configParameterForm.get(`${useCaseId}.${functionControl}`).setValue(value);
        }
        else {
          this.setDialogData(
            MODAL_TYPE.DATA_INCORRECT_ERROR.name,
            MODAL_TYPE.DATA_INCORRECT_ERROR.heading,
            MODAL_TYPE.DATA_INCORRECT_ERROR.message,
          )
        }
        // this.configParameterForm.controls[].setValue(value)

      }
      else {
        this.showDialog = true;

      }
    }
  }
  insertValuesIntocommTriggerForm(commTriggerObj: any){
    let commTriggerList = commTriggerObj?.['communication-trigger-list'];
    let stopAllComms = commTriggerObj?.['stop-all-communication'];

    if(stopAllComms){
      this.commTriggerForm.get('UC_01.UC_01').setValue(true);
    }
    else {
      this.commTriggerForm.get('UC_01.UC_01').setValue(false);
    }
    for(let commTriggerIndex: number =0; commTriggerIndex< commTriggerList.length;commTriggerIndex++){
      if(commTriggerList[commTriggerIndex].hasOwnProperty('use-case-id') && commTriggerList[commTriggerIndex].hasOwnProperty('functionality-id')){
        let commId = commTriggerList[commTriggerIndex]['use-case-id'];
        let functionalityId = commTriggerList[commTriggerIndex]['functionality-id'];
        let functionControl = this.commTriggerMapping[`${commId}`][`${functionalityId}`];
        let value = commTriggerList[commTriggerIndex]['frequency-days'];

        if(commTriggerList[commTriggerIndex]?.['is-active'] == 'Y'){
          this.commTriggerForm.get(`${commId}.${commId}`).setValue(true);
        }
        else {
          this.commTriggerForm.get(`${commId}.${commId}`).setValue(false);
        }
        if(Array.isArray(value)){
          for(let i in value){
            if(commId == 'UC_27' || commId == 'UC_29'){
              for(let optionDayIndex in this.optionDays){
                  if(this.optionDays[optionDayIndex].value == value[i])
                  this.commTriggerForm.get(`${commId}.${functionControl}.${String(optionDayIndex)}`).setValue(true)
              }
            }
            else {
              this.commTriggerForm.get(`${commId}.${functionControl}`).setValue(String(value[i]));
            }
          }
        }
        else{
              this.setDialogData(
                MODAL_TYPE.DATA_FETCH_ERROR.name,
                MODAL_TYPE.DATA_FETCH_ERROR.heading,
                MODAL_TYPE.DATA_FETCH_ERROR.message,
              )
        }
      }
      else{
        this.setDialogData(
          MODAL_TYPE.DATA_FETCH_ERROR.name,
          MODAL_TYPE.DATA_FETCH_ERROR.heading,
          MODAL_TYPE.DATA_FETCH_ERROR.message,
        )
      }
    }
  }
  
  updatedValuesFromCustomerForm(): adminTypeCustomer[]{
    let result: adminTypeCustomer[] = [];
    for(let customerType of ['HNI', 'MED', 'LOW']){
      let obj = <adminTypeCustomer>{};
      obj['max-annual-premium'] = Number(this.customerTypeConfigForm.get(`${customerType}_AP_max`).value);
      obj['min-annual-premium'] = Number(this.customerTypeConfigForm.get(`${customerType}_AP_min`).value);
      obj['max-total-sum-assured'] = Number(this.customerTypeConfigForm.get(`${customerType}_TSA_max`).value);
      obj['min-total-sum-assured'] = Number(this.customerTypeConfigForm.get(`${customerType}_TSA_min`).value);
      if(customerType == 'MED') customerType = 'Medium';
      if(customerType == 'LOW') customerType = 'Low';
      obj['type-of-customer'] = customerType;
      result.push(obj);
    }
    return result;
  }

  updatedValuesFromUseCaseForm(): adminCount[]{
    let result: adminCount[] = [];
    for(let key in this.useCaseFunctionalityMapping){
      for(let functionalityId in this.useCaseFunctionalityMapping[key]){
        let functionalId = this.useCaseFunctionalityMapping[key][functionalityId]
        if(this.configParameterForm.get(`${key}.${functionalId}`).touched){
          let obj= <adminCount>{};
          obj.count = Number(this.configParameterForm.get(`${key}.${functionalId}`).value);
          obj['functionality-id'] = String(functionalityId);
          obj['use-case-id']=String(key);
          result.push(obj);
        }
      }
    }
    return result;
  }
  updatedValuesFromCommTriggerForm(): commTrigger[]{
    let result: commTrigger[] = [];
    for(let key in this.commTriggerMapping){
      let obj = <commTrigger>{};
      obj['use-case-id'] = key;
      obj['is-active'] = this.commTriggerForm.get(`${key}.${key}`).value ? 'Y':'N';
      let arr:Number[] = [];
      for(let functionalityId in this.commTriggerMapping[key]){
        let functionalId =  this.commTriggerMapping[key][functionalityId];
        if(key=='UC_27' || key=='UC_29'){
          for(let i in this.optionDays){
            let control = this.commTriggerForm.get(`${key}.${functionalId}`).get(i);
            if(control.value){
              arr.push(Number(this.optionDays[i].value))
            }
          }
        }
        else{
          arr.push(Number(this.commTriggerForm.get(`${key}.${functionalId}`).value))
        }
      }
      obj['values'] = arr;
      result.push(obj);
    }
    return result;
  }


  onSave(event: any){
    if(!Object.values(this.formEnable).includes(true)){
      //Release a prompt screen if no form is enabled to save
      this.setDialogData(
        MODAL_TYPE.FORM_ENABLE_ERROR.name,
        MODAL_TYPE.FORM_ENABLE_ERROR.heading,
        MODAL_TYPE.FORM_ENABLE_ERROR.message
      )
    }
    else if(Object.keys(this.formEnable).filter(key=>event.target.id.includes(key) && this.formEnable[key]).length == 0){
      //Release a prompt screen if save button pressed for disabled form.
      const formName = Object.keys(this.formEnable).filter(key=>this.formEnable[key])[0];
      this.setDialogData(
        MODAL_TYPE.FORM_SAVE_ERROR.name,
        MODAL_TYPE.FORM_SAVE_ERROR.heading,
        MODAL_TYPE.FORM_SAVE_ERROR.message+` ${formName} form.`
      )
    }
    else if(this.formEnable[this.sectionId.CUSTOMER_TYPE_CONFIG] && event.target.id.includes(this.sectionId.CUSTOMER_TYPE_CONFIG)){
      //Check if form is customer type form is enabled and save button clicked for customer form
      if(this.customerTypeConfigForm.status == "INVALID"){
        //Check if form is in valid state
        this.setDialogData(
          MODAL_TYPE.DATA_VALIDATION_ERROR.name,
          MODAL_TYPE.DATA_VALIDATION_ERROR.heading,
          MODAL_TYPE.DATA_VALIDATION_ERROR.message
        )
      }
      else{
      this.formEnable[this.sectionId.CUSTOMER_TYPE_CONFIG] =  false;
      this.customerTypeConfigForm.disable();
      const result = this.updatedValuesFromCustomerForm();
      this.customerService.putCustomerFormData(result).subscribe({
        next:response=>{
          //If response Success then show form successfully submitted successfully otherwise show error response to user
          if(response && response.hasOwnProperty('status')){
            if(response['response-code'].toLowerCase()=='200'){
              this.setDialogData(
                MODAL_TYPE.DATA_SAVE_SUCCESS.name,
                MODAL_TYPE.DATA_SAVE_SUCCESS.heading,
                MODAL_TYPE.DATA_SAVE_SUCCESS.message
              )
              this.customerService.getCustomerData().subscribe({
                next:response=>{
                  if(response['response-code'].toLowerCase()=='200'){
                    this.insertValuesIntoCustomerForm(response?.response);
                  }
                  else{
                    this.setDialogData(MODAL_TYPE.DATA_FETCH_ERROR.name,MODAL_TYPE.DATA_FETCH_ERROR.heading,MODAL_TYPE.DATA_FETCH_ERROR.message )
                  }
                }, 
                error:(e)=>this.handleGetError(e)});
  
            }
          }
        },
        error: err=>{
          this.setDialogData(
            MODAL_TYPE.DATA_SAVE_ERROR.name,
            MODAL_TYPE.DATA_SAVE_ERROR.heading,
            MODAL_TYPE.DATA_SAVE_ERROR.message
          )
        }})
      }
    }
    else if(this.formEnable[this.sectionId.USE_CASE_CONFIGURATION_PARAMTERS] && event.target.id.includes(this.sectionId.USE_CASE_CONFIGURATION_PARAMTERS)){
        this.configParameterForm.disable();
        this.formEnable[this.sectionId.USE_CASE_CONFIGURATION_PARAMTERS] =  false;
        const result = this.updatedValuesFromUseCaseForm();
        this.customerService.putUseCaseFormData(result).subscribe({
          next:response =>{
          //If response Success then show form successfully submitted successfully otherwise show error response to user
          if(response['response-code'].toLowerCase()=='200'){
            this.setDialogData(
              MODAL_TYPE.DATA_SAVE_SUCCESS.name,
              MODAL_TYPE.DATA_SAVE_SUCCESS.heading,
              MODAL_TYPE.DATA_SAVE_SUCCESS.message
            )
            this.customerService.getuseCaseData().subscribe({
              next:response=>{
                  if(response["response-code"].toLowerCase()=='200'){
                    this.insertValuesIntoUseCaseForm(response?.response);
                  }
                  else{
                    this.setDialogData(MODAL_TYPE.DATA_FETCH_ERROR.name,MODAL_TYPE.DATA_FETCH_ERROR.heading,MODAL_TYPE.DATA_FETCH_ERROR.message )
                  }
                }, 
                error:(e)=>this.handleGetError(e)});

          }
          else{
            this.setDialogData(MODAL_TYPE.DATA_SAVE_ERROR.name,MODAL_TYPE.DATA_SAVE_ERROR.heading,MODAL_TYPE.DATA_SAVE_ERROR.message)
          }
        }, 
        error:error=>{
          this.setDialogData(MODAL_TYPE.DATA_SAVE_ERROR.name,MODAL_TYPE.DATA_SAVE_ERROR.heading,MODAL_TYPE.DATA_SAVE_ERROR.message)
        }})
      }
    else if(this.formEnable[this.sectionId.COMMUNICATION_TRIGGERS] && event.target.id.includes(this.sectionId.COMMUNICATION_TRIGGERS)){
        if(this.commTriggerForm.status == "INVALID"){
          //Check if form is in valid state
          this.setDialogData(
            MODAL_TYPE.DATA_VALIDATION_ERROR.name,
            MODAL_TYPE.DATA_VALIDATION_ERROR.heading,
            MODAL_TYPE.DATA_VALIDATION_ERROR.message
          )
        }
        else{
        this.commTriggerForm.disable();
        this.formEnable[this.sectionId.COMMUNICATION_TRIGGERS] =  false;
        const result  = this.updatedValuesFromCommTriggerForm();
        const finalResult  = {
          "stop-all-communication": this.commTriggerForm.get(`UC_01.UC_01`).value,
          "communication-trigger-list": [...result]
        }
        this.customerService.putCommTriggerFormData(finalResult).subscribe({
          next:response=>{
          this.setDialogData(
            MODAL_TYPE.DATA_SAVE_SUCCESS.name,
            MODAL_TYPE.DATA_SAVE_SUCCESS.heading,
            MODAL_TYPE.DATA_SAVE_SUCCESS.message
          )
          if(response['response-code'].toLowerCase()=='200'){
            this.customerService.getcommTriggerData().subscribe({
              next:response=>{
                if(response['response-code'].toLowerCase()=='200'){
                  this.insertValuesIntocommTriggerForm(response?.response);
                }
                else{
                  this.setDialogData(MODAL_TYPE.DATA_FETCH_ERROR.name,MODAL_TYPE.DATA_FETCH_ERROR.heading,MODAL_TYPE.DATA_FETCH_ERROR.message )
                }
              }, 
              error:(e)=>this.handleGetError(e)});
          }
          }, 
          error:error=>{
            this.setDialogData(
              MODAL_TYPE.DATA_SAVE_ERROR.name,
              MODAL_TYPE.DATA_SAVE_ERROR.heading,
              MODAL_TYPE.DATA_SAVE_ERROR.message
            )
          }
        })
      }
    }
    else {
      this.showDialog = true;
    }
  }

  onEdit(event: any){
    if(Object.values(this.formEnable).includes(true)){
      //release a prompt screen one form is already enabled can't enable second form
      const formName = Object.keys(this.formEnable).filter(key=>this.formEnable[key])[0];
      this.setDialogData(
        MODAL_TYPE.FORM_EDIT_ERROR.name,
        MODAL_TYPE.FORM_EDIT_ERROR.heading,
        MODAL_TYPE.FORM_EDIT_ERROR.message+` ${formName} form.`)
    }
    else {
      if(event.target.id.includes(this.sectionId.CUSTOMER_TYPE_CONFIG)){
        this.customerTypeConfigForm.enable();
        this.formEnable[this.sectionId.CUSTOMER_TYPE_CONFIG] =  true;
      }
      else if(event.target.id.includes(this.sectionId.USE_CASE_CONFIGURATION_PARAMTERS)){
        this.configParameterForm.enable();
        this.formEnable[this.sectionId.USE_CASE_CONFIGURATION_PARAMTERS] =  true;
      }
      else if(event.target.id.includes(this.sectionId.COMMUNICATION_TRIGGERS)){
        this.commTriggerForm.enable();
        this.formEnable[this.sectionId.COMMUNICATION_TRIGGERS] =  true;
      }
    }
  }

  onCancel(event: any){
    // if(!Object.values(this.formEnable).includes(true) || Object.keys(this.formEnable).filter(key=>this.formEnable[key])[0] != event.target.id){
      if(Object.keys(this.formEnable).filter(key=>event.target.id.includes(key) && this.formEnable[key]).length == 0){
      //Release a prompt screen if form is not enabled or button clicked for another form
      this.setDialogData(
        MODAL_TYPE.FORM_CANCEL_ERROR.name,
        MODAL_TYPE.FORM_CANCEL_ERROR.heading,
        MODAL_TYPE.FORM_CANCEL_ERROR.message
      )
    }
    else{

    this.setDialogData(
      MODAL_TYPE.FORM_CANCEL_ALERT.name,
      MODAL_TYPE.FORM_CANCEL_ALERT.heading,
      MODAL_TYPE.FORM_CANCEL_ALERT.message
    )
    this.cancelEventId = event.target.id;
    }
  }
  confirmCancel(event){
      this.cleanLoad();
      if(event.target.id.includes(this.sectionId.CUSTOMER_TYPE_CONFIG)){
        this.isLoading=true;
        //Call Single Data API and Load Data
        this.customerService.getCustomerData().subscribe({
          next:(response)=>{
          if(response?.status.toLowerCase() == 'success'){
            this.isLoading=false;
            this.insertValuesIntoCustomerForm(response?.response);
          }
          else {
            this.isLoading=false;
            this.setDialogData(
              MODAL_TYPE.DATA_FETCH_ERROR.name,
              MODAL_TYPE.DATA_FETCH_ERROR.heading,
              MODAL_TYPE.DATA_FETCH_ERROR.message,
            )
          }
          }, 
          error:(e)=>this.handleGetError(e)});

      }
      else if(event.target.id.includes(this.sectionId.USE_CASE_CONFIGURATION_PARAMTERS)){
        //Call Single Data API and Load Data
        this.isLoading = true;
        this.customerService.getuseCaseData().subscribe({
          next:(response)=>{
          if(response?.status.toLowerCase() == 'success'){
            this.isLoading=false;
            this.insertValuesIntoUseCaseForm(response?.response);
          }
          else {
            this.isLoading=false;
            this.setDialogData(
              MODAL_TYPE.DATA_FETCH_ERROR.name,
              MODAL_TYPE.DATA_FETCH_ERROR.heading,
              MODAL_TYPE.DATA_FETCH_ERROR.message,
            )
          }
          }, 
          error:(e)=>this.handleGetError(e)})
      }
      else if(event.target.id.includes(this.sectionId.COMMUNICATION_TRIGGERS)){
        //Call Single Data API and Load Data
        this.isLoading = true;
        this.customerService.getcommTriggerData().subscribe({
          next:(response)=>{
          if(response?.status.toLowerCase() == 'success'){
            this.isLoading=false;
            this.insertValuesIntocommTriggerForm(response?.response);
          }
          else {
            this.isLoading=false;
            this.setDialogData(
              MODAL_TYPE.DATA_FETCH_ERROR.name,
              MODAL_TYPE.DATA_FETCH_ERROR.heading,
              MODAL_TYPE.DATA_FETCH_ERROR.message,
            )
          }
      }, 
          error:(e)=>this.handleGetError(e)});
      }
      this.closeDialog();
  }
  setDialogData(name: string, heading: string, message: string){
    this.showDialog = true;
    this.dialogData.name = name;
    this.dialogData.heading =  heading;
    this.dialogData.message = message;
  }

  closeDialog(){
    this.showDialog = false;
    this.dialogData.name = "";
    this.dialogData.message ="";
    this.dialogData.heading ="";
  }

  signout(){
    this.customerService.postSignout();
    this.router.navigate(['/']);
  }

  handleGetError(err){
    this.isLoading=false;
            if(err.status == '401'){
              this.setDialogData(
                MODAL_TYPE.FORM_UNAUHTORIZED_ERROR.name,
                MODAL_TYPE.FORM_UNAUHTORIZED_ERROR.heading,
                MODAL_TYPE.FORM_UNAUHTORIZED_ERROR.message,
              )
            }
            else{
            this.setDialogData(
              MODAL_TYPE.DATA_FETCH_ERROR.name,
              MODAL_TYPE.DATA_FETCH_ERROR.heading,
              MODAL_TYPE.DATA_FETCH_ERROR.message,
            )}
  }

  cleanLoad(){
    this.commTriggerForm.disable();
    this.customerTypeConfigForm.disable();
    this.configParameterForm.disable();
    Object.keys(this.formEnable).forEach((form)=>{
      this.formEnable[form]=false
    })
  }
}
