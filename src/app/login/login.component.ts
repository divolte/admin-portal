import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { CustomerService } from '../shared/services/customer.service';
import MODAL_TYPE from '../../assets/modal.type.json';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isUserValid = true;
  showDialog=false;
  loginForm: FormGroup;

  dialogData = {
    name: '',
    message: '',
    heading: ''
  }

  closeDialog(){
    this.showDialog = false;
    this.dialogData.name = "";
    this.dialogData.message ="";
    this.dialogData.heading ="";
  }

  setDialogData(name: string, heading: string, message: string){
    this.showDialog = true;
    this.dialogData.name = name;
    this.dialogData.heading =  heading;
    this.dialogData.message = message;
  }
  errorAction(error:any){
    if(error?.error?.status == '401'){
      this.setDialogData(
        MODAL_TYPE.LOGIN_ERROR.name,
        MODAL_TYPE.LOGIN_ERROR.heading,
        MODAL_TYPE.LOGIN_ERROR.message,
        )
     }
    else {
      this.setDialogData(
        MODAL_TYPE.DATA_SAVE_ERROR.name,
        MODAL_TYPE.DATA_SAVE_ERROR.heading,
        MODAL_TYPE.DATA_SAVE_ERROR.message,
      )
    }
    this.loginForm.enable();  

  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
) {
    // redirect to home if already logged in
    if (this.customerService.getAuthenticationToken()) { 
        this.router.navigate(['/dashboard']);
    }
}

ngOnInit() {
  this.loginForm= new FormGroup({
    username: new FormControl('', [Validators.required]),
  	password: new FormControl('', [Validators.required]),
    });
}

onSubmit() {

  //if form is invalid do nothing
  if(this.loginForm.invalid){
    return ;
  }
  this.loginForm.disable();
  //submit post request to server for login
  // this.customerService.getAuthorization().subscribe(response=>{
  //   localStorage.setItem('currentAuthorizationToken',response?.access_token)
    this.customerService.postLogin(this.loginForm.value.username,this.loginForm.value.password)
    .subscribe(response =>{
      if(response && response.status.toLowerCase() == 'sucess'){
        // localStorage.setItem('currentUserToken',response.token);
        localStorage.setItem('username',this.loginForm.value.username)
        localStorage.setItem('currentAuthenticationToken',response?.token);
        this.router.navigate(['/dashboard']);
      }
    }, error=>this.errorAction(error))


}


}
